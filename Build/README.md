This is a file that makes sure when I pull my repo, it has an otherwise empty Build directory in it for my own convenience.

All other files in this directory will be ignored.
