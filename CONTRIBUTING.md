Contributions are welcome. There are some caveats, though.

dfsgok-bearlibterminal is maintained in a publicly viewable repository using
the Git distributed version control system (DVCS). Git repositories use
cryptographic hashes to ensure the integrity of both data and metadata, and
certain personal identifying information (as defined by the EU GDPR) is stored
in the metadata.

As such, you have three options when considering whether to contribute changes
to dfsgok-bearlibterminal:

### Option 1: Request anonymity at the time of contribution

If you wish to have your contributions be kept anonymous, clearly state when
contributing that you wish your contributions to be anonymous, and submit them
through channels other than the Gitlab pull request system.

### Option 2: Accept permanent attribution

1. Acknowledge that you have read and understood this document.
2. Accept that by having your contributions accepted under your chosen identity you are effectively publishing that PII to the entire world.
3. Agree that your contributions may be attributed to you indefinitely using the identity under which you submitted those contributions.

### Option 3: Don't contribute after all

If you don't like either of the first two options, you can instead choose to
not contribute.

