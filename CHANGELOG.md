### 0.1.0 (2018-09-01)

- Project created from version 0.15.7 of the original BearLibTerminal
- Original upstream changelog for 0.15.7 preserved as upstream-CHANGELOG.md
- For DFSG compliance reasons, this fork does not contain the NanoJPEG library and has no support for JPEG decoding.

