BearLibTerminal provides a pseudoterminal window with a grid of character cells and a simple yet powerful API for flexible textual output and uncomplicated input processing.

dfsgok-bearlibterminal is a fork of BearLibTerminal which excludes support for JPEG decoding. If at some point in the future I have enough spoons, I will also remove the embedded PicoPNG and FreeType code copies.

I have removed the usage notes for languages I haven't tested the bindings for.

You can find the original version (which cannot be redistributed commercially, as the licence for NanoJPEG prohibits charging a fee for distribution) on [Bitbucket](https://bitbucket.org/cfyzium/bearlibterminal/)

### Features

* Unicode support: you can use UTF-8 or UTF-16/32 wide strings easily.
* Support for bitmap and vector (TrueType) fonts and tilesets.
* Extended output facilities: tile composition, alignment, offsets.
* High performance (uses OpenGL).
* Keyboard and mouse support.
* Bindings for several programming languages: C/C++, C#, Go, Lua, Pascal, Python, Ruby.
* Windows, Linux and OS X support.


### Documentation

The original upstream documentation remains applicable in the majority of regards except that there is no support for loading JPEG images and I have renamed the library and the main header file.

* [Reference](http://foo.wyrd.name/en%3Abearlibterminal%3Areference) (functions, constants, behaviour)
* [Design overview](http://foo.wyrd.name/en%3Abearlibterminal%3Adesign)


### Using

Some notes about using it with various languages or compilers:


#### C/C++

Visual C++ projects should be linked against BearLibTerminal.lib import library (specify it in the additional linker dependencies).

MinGW projects should link against .dll directly (the .lib is just an import library for Visual C++, do not copy it):

    g++ -I/path/to/header -L/path/to/dll main.cpp -ldfsgok-bearlibterminal -o app.exe

#### Lua

Wrapper for Lua is built-in. You need to use a regular (dynamic) Lua runtime
and place dfsgok-bearlibterminal binary in a suitable location (e. g. in the
same directory as script). For Linux you'll also need to rename the .so to just
'dfsgok-bearlibterminal.so' (dropping the 'lib' prefix). After that it would be
possible to import the library the usual way:

    local terminal = require "dfsgok-bearlibterminal" 

### Building

I do not provide pre-built binaries of dfsgok-bearlibterminal.

To build dfsgok-bearlibterminal you will need CMake and a recent GCC/MinGW compiler. For Linux any GCC version 4.6.3 and above will do. For Windows there are several MinGW builds with various quirks, using [TDM-GCC](http://tdm-gcc.tdragon.net/) or [mingw-builds](http://mingw-w64.org/doku.php/download/mingw-builds) (a flavour of mingw-w64) is recommended.  MinGW compiler MUST use Posix thread model.


### License

The library is licensed mainly under the MIT license with a few parts under other permissive licenses.
